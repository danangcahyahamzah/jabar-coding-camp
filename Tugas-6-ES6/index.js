/*
// so'al 1
buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini

*/
//.... jawaban 1
let panjang = 0;
let lebar = 0;
let luasPersegiPanjang = ( panjang , lebar) => 'luas persegi panjang adalah ' + panjang * lebar ;

const KelilingPersegiPanjang = (panjang , lebar) => 'keliling peersegi panjang adalah ' + 2 * (panjang + lebar)

console.log(luasPersegiPanjang (5 , 3));
console.log(KelilingPersegiPanjang (5 , 3))



/*
// so'al 2


Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana

 

const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 
*/

//.... jawaban 2

const newFunction = (firstName , lastName) => firstName + ' ' + lastName

console.log(newFunction("William" , "Imoh"));


/*
//so'al 3

Diberikan sebuah objek sebagai berikut:

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:

const firstName = newObject.firstName;
const lastName = newObject.lastName;
const address = newObject.address;
const hobby = newObject.hobby;
Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)

// Driver code
console.log(firstName, lastName, address, hobby)


*/
//jawaban 2

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

const {firstName , lastName , address , hobby} = newObject

console.log(firstName, lastName, address, hobby)


/*
//so'al 4

//Kombinasikan dua array berikut menggunakan array spreading ES6

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)
*/

// jawaban 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east]

console.log(combined);


/*
//so'al 5

//sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

 

const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet

console.log(before);

*/

//jawaban 5

const planet = "earth" 
const view = "glass" 

const before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`

console.log(before);